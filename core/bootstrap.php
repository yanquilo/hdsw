<?php 
require_once "App.php";
require_once "Request.php";
require __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/../utils/MyLog.php";

session_start();

$logger = MyLog::load("logs/info.log");

App::bind("logger", $logger);

$config = require_once __DIR__ . "/../app/config.php";

App::bind("config", $config);
