<?php
require_once "core/App.php";
require_once "exceptions/QueryExceptions.php";
require_once "exceptions/NotFoundException.php";

abstract class QueryBuilder{

    private $connection;

    private $table;

    private $classEntity;

    public function __construct(string $table, string $classEntity)

    {

        $this->connection = App::getConnection();

        $this->table = $table;

        $this->classEntity = $classEntity;
    }

    public function findAll()

    {

        $sql = "SELECT * FROM $this->table";

        $result = $this->executeQuery($sql);

        //if (empty($result)) {
           // throw new QueryException("Registro no encontrado");
      //   }

        return $result;
       
    } 


    public function save(IEntity $entity): void

    {

        try {

            $parameters = $entity->toArray();

            $sql = sprintf(
                    "insert into %s (%s) values (%s)",

                    $this->table,

                    implode(", ", array_keys($parameters)),

                    ":" . implode(", :", array_keys($parameters))

                );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch (PDOException $exception) {

            throw new QueryException("Error al insertar en la BBDD.");
        }
    }

    public function executeQuery(string $sql): array

    {

        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute() === false)

        throw new QueryException("No se ha podido ejecutar la consulta");

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    public function find(int $id): IEntity

    {

        $sql = "SELECT * FROM $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if (empty($result))

        throw new NotFoundException("No se ha encontrado el elemento con id $id");



        return $result[0];
    }


}
