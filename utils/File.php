<?php
class File
{
    private $file;

    private $filename;


    public function __construct(string $filename, array $arrTypes)
    {

        $this->file = $_FILES[$filename];

        $this->filename = "";

        if (($this->file["name"] == "")) {

            throw new FileException("Debes especificar un fichero", 1);

        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    throw new FileException("El fichero es demasiado grande", 2);

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("Archivo incompleto", 2);

                default:

                    throw new FileException("Error al subir el fichero", 2);

                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {

            throw new FileException("El tipo de archivo no es compatible", 3);

        }
    }


    public function saveUploadFile(string $ruta)
    {
        $rutaFinal = $ruta . $this->file["name"];
        
        if (is_uploaded_file($this->file["tmp_name"]) === false) {
            throw new FileException("El archivo no se ha subido mediante formulario");
        }
        if (is_file($rutaFinal)) {
            $idUnico = time();
            $this->file["name"] = $idUnico . $this->file["name"];
            $rutaFinal = $ruta . $this->file["name"];
        }
        if (
            move_uploaded_file($this->file["tmp_name"], $rutaFinal) === false
        ) {
            throw new FileException("No se ha podido mover el fichero al destino especificado");
        }
    }

    public function copyFile($origen, $destino)
    {
        if (is_file($origen . $this->file["name"]) == false) {
            throw new FileException("No existe el fichero" . $origen);
        }
        if (is_file($destino . $this->file["name"])) {
            throw new FileException("Ya existe el fichero" . $origen);
        } else {
            if (copy($origen . $this->file["name"], $destino . $this->file["name"]) == false) {
                throw new FileException("No se ha podido copiar el fichero.");
            }
        }
    }


    public function getFilename()
    {
        return $this->file["name"];
    }

}
?>