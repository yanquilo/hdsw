<?php
require_once "utils/utils.php";
require_once "exceptions/FileException.php";
require_once "entity/CardsOpinion.php";
require_once "utils/File.php";
require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "repository/CardsOpinionRepository.php";
require_once "core/FashMessage.php";

use Monolog\Logger ;
use Monolog\Handler\StreamHandler ;

$log = new Logger ('cards');
$log -> pushHandler ( new StreamHandler ('logs/info.log ', Logger :: INFO ));

$mensaje = FlashMessage::get("mensaje");
$nombre = FlashMessage::get("nombre");
$fecha = FlashMessage::get ("fecha");
$descripcion = FlashMessage::get ("descripcion");
//$config = require_once("app/config.php");

    try {

    //$connection = Connection::make($config["database"]);


    $cardsOpinionRepository = new CardsOpinionRepository();

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $nombre = trim(htmlspecialchars($_POST["nombre"]));
        FlashMessage::set("nombre", $nombre);
        $fecha = trim(htmlspecialchars($_POST["fecha"]));
        FlashMessage::set("fecha", $fecha);
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        FlashMessage::set("descripcion", $descripcion);

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $image = new File("image", $tiposAceptados);

        $image->saveUploadFile(CardsOpinion::RUTA_CARDS_IMAGES);
        $image->copyFile(CardsOpinion::RUTA_CARDS_IMAGES, CardsOpinion::RUTA_CARDS_COPY);

        $cardsOpinion = new CardsOpinion(0,$image->getFilename(), $nombre, $fecha, $descripcion);

        $cardsOpinionRepository->save($cardsOpinion);

        FlashMessage::set("mensaje", "Se ha guardado la imagen en la BBDD.");

        $log->info($mensaje);

        App::get("logger")->add($mensaje);

        FlashMessage::unset("nombre");

        FlashMessage::unset("fecha");

        FlashMessage::unset("descripcion");

        $nombre = "";

        $fecha = "";

        $descripcion = "";

    } //final del fin

   
    $cards = $cardsOpinionRepository->findAll();

    } catch (FileException $fileException) {

    //$errores[] = $fileException->getMessage();
    FlashMessage::set("errores", [$fileException->getMessage()]);

    } catch (QueryException $queryEception) {

    //$errores[] = $queryEception->getMessage();
    FlashMessage::set("errores", [$queryEception->getMessage()]);
    
    } catch (AppException $appException) {

    //$errores[] = $appException->getMessage();
    FlashMessage::set("errores", [$appException->getMessage()]);
}


//Fichero subido
$mensaje = FlashMessage::get("mensaje");

unset($_SESSION["mensaje"]);

//Error al subir el fichero
$errores = FlashMessage::get("errores");

unset($_SESSION["errores"]);

require __DIR__ . "/../views/cards.view.php";
