<?php
require_once "utils/utils.php";
// variables
$nameErr = $emailErr = $apellidoErr = $subjectErr = "";
$name = $email = $apellido = $subject = "";

//inicialización
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Nombre
    if (empty($_POST["nombre"])) {
        $nameErr = "Nombre requerido";
    }

    //Email
    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
    } else
        // check if e-mail address is well-formed
        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
        }

    //Apellido
    if (empty($_POST["apellido"])) {
        $apellidoErr = "Apellido requerido";
    }

    //Comprobar que el apellido sean letras 
    if (is_string($apellido)) {
        $apellido = $apellido;
    } else {
        $apellidoErr = "El apellido solo admite letras";
    }

    //Asunto
    if (empty($_POST["title"])) {
        $subjectErr = "Asunto requerido";
    }
}


require __DIR__ . "/../views/contact.views.php";
?>