<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav-doc.part.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container"> <br><br><br>
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>Cards</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>

                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Foto</label>
                        <input class="form-control-file" name="image" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="nombre">Nombe:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="fecha">Fecha:</label>
                        <input type="date" class="form-control" name="fecha">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion"></textarea><br><br>
                        <button style="background-color: aquamarine;" class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div><br><br>

        <table style="border: 2px solid black;" class="table">
            <th style="background-color: black; color:aquamarine" scope="col">ID</th>
            <th style="background-color: black; color:aquamarine" scope="col">FOTO</th>
            <th style="background-color: black; color:aquamarine" scope="col">NOMBRE</th>
            <th style="background-color: black; color:aquamarine" scope="col">FECHA</th>
            <th style="background-color: black; color:aquamarine" scope="col">DESCRIPCION</th>

            <?php if (isset($cards)) {
            ?>

                <?php foreach ($cards as $imagen) : ?>

                    <tr>
                        <th scope="row"><?= $imagen->getId() ?></th>
                        <td>
                            <img src="<?= $imagen->getURLPortfolio() ?>" alt="" title="<?= $imagen->getFoto() ?>" width="15%">
                        </td>
                        <td><?= $imagen->getNombre() ?></td>
                        <td><?= $imagen->getFecha() ?></td>
                        <td><?= $imagen->getDescripcion() ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php } ?>
        </table>

    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>