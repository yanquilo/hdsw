<?php include __DIR__ . "/partials/inicio-doc.part.php"; ?>
<?php include __DIR__ . "/partials/nav-doc.part.php"; ?>

<div class="hero-wrap" style="background-image: url('images/bg_3.jpg');">
  <div class="overlay"></div>
  <div class="container">
    <div class="row no-gutters slider-text d-flex align-itemd-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center d-flex align-items-end justify-content-center">
        <div class="text">
          <p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.php">Home</a></span> <span>Contact Us</span></p>
          <h1 class="mb-4 bread">Contact Us</h1>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section contact-section bg-light">
  <div class="container">
    <div class="row d-flex mb-5 contact-info">
      <div class="col-md-12 mb-4">
        <h2 class="h3">Contact Information</h2>
      </div>
      <div class="w-100"></div>
      <div class="col-md-3 d-flex">
        <div class="info rounded bg-white p-4">
          <p><span>Address:</span> 198 West 21th Street, Suite 721 New York NY 10016</p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info rounded bg-white p-4">
          <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info rounded bg-white p-4">
          <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@yoursite.com</a></p>
        </div>
      </div>
      <div class="col-md-3 d-flex">
        <div class="info rounded bg-white p-4">
          <p><span>Website</span> <a href="#">yoursite.com</a></p>
        </div>
      </div>
    </div>
    <div class="row block-9">
      <div class="col-md-6 order-md-last d-flex">
        <form action="contact.php" class="bg-white p-5 contact-form" method="post">
          <div class="form-group">
            <input name="nombre" type="text" class="form-control" placeholder="Your Name" value="<?= $name ?>">
            <span class="error">* <?php echo $nameErr; ?></span>
          </div>
          <div class="form-group">
            <input name="apellido" type="text" class="form-control" placeholder="Your LastName" value="<?= $apellido ?>">
            <span class="error">* <?php echo $apellidoErr; ?></span>
          </div>
          <div class="form-group">
            <input name="email" type="text" class="form-control" placeholder="Your Email" value="<?= $email ?>">
            <span class="error">* <?php echo $emailErr; ?></span>
          </div>
          <div class="form-group">
            <input name="title" type="text" class="form-control" placeholder="Subject" value="<?= $subject ?>">
            <span class="error">* <?php echo $subjectErr; ?></span>
          </div>
          <div class="form-group">
            <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
          </div>
        </form>

      </div>

      <div class="col-md-6 d-flex">
        <div id="map" class="bg-white"></div>
      </div>
    </div>
  </div>
</section>



<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>