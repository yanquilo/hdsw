   <!--Inicio del nav-->
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        <div class="container">
            <a class="navbar-brand" href="index.php">Harbor<span>lights</span></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="oi oi-menu"></span> Menu
            </button>

            <div class="collapse navbar-collapse" id="ftco-nav">
                <ul class="navbar-nav ml-auto">
                    <li class=" lien <?php if(esactiva('index')){echo 'active';} ?>"><a href="<?php if(esactiva('index')){echo'#';}else{echo'index';} ?>" nav-item active class="nav-link">Home</a></li>
                    <li class=" lien <?php if(esactiva('rooms')){echo 'active';} ?>"><a href="<?php if(esactiva('rooms')){echo'#';}else{echo'rooms';} ?>" nav-item class="nav-link">Our Rooms</a></li>
                    <li class=" lien <?php if(esactiva('restaurant')){echo 'active';} ?>"><a href="<?php if(esactiva('restaurant')){echo'#';}else{echo 'restaurant';} ?>" nav-item class="nav-link">Restaurant</a></li>
                    <li class=" lien <?php if(esactiva('about')){echo 'active';} ?>"><a href="<?php if(esactiva('about')){echo'#';}else{echo 'about';} ?>" nav-item class="nav-link">About Us</a></li>
                    <li class=" lien <?php if(esactiva('cards')){echo 'active';} ?>"><a href="<?php if(esactiva('cards')){echo'#';}else{echo 'cards';} ?>" nav-item class="nav-link">Cards</a></li>
                    <li class=" lien <?php if(esactiva('contact')){echo 'active';} ?>"><a href="<?php if(esactiva('contact')){echo'#';}else{echo 'contact';} ?>" nav-item class="nav-link">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- END nav -->