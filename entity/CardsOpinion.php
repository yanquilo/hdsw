<?php
require_once "database/IEntity.php";

class CardsOpinion implements IEntity
{

    //ATRIBUTOS
    private $id;
    private $foto;
    private $nombre;
    private $fecha;
    private $descripcion;

    //Constantes
    const RUTA_CARDS_IMAGES = "images/portfolio/";

    const RUTA_CARDS_COPY = "images/copy/";

    //CONSTRUCTOR
    public function __construct($id = 0, string $foto = "", string $nombre = "", string $fecha = "", string $descripcion = "")
    {
        $this->id = $id;
        $this->foto = $foto;
        $this->nombre = $nombre;
        $this->fecha = $fecha;
        $this->descripcion = $descripcion;
    }

    public function toArray(): array

    {

        return [

                "id" => $this->getId(),

                "nombre" => $this->getNombre(),

                "descripcion" => $this->getDescripcion(),

                "foto" => $this->getFoto(),

                "fecha" => $this->getFecha()

            ];
    }

    public function getURLPortfolio(): string

    {

        return self::RUTA_CARDS_IMAGES . $this->getFoto();
    }

    public function getURLCopy(): string

    {

        return self::RUTA_CARDS_COPY . $this->getFoto();
    }

    //GETTER AND SETTER

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of foto
     */ 
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set the value of foto
     *
     * @return  self
     */ 
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }


    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }
}
?>